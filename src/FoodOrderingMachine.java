import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton sandButton;
    private JButton burgerButton;
    private JButton toastButton;
    private JButton coffeeButton;
    private JButton hotdogButton;
    private JButton chickenButton;
    private JLabel order;
    private JTextPane ordersList;
    private JLabel total;
    private JButton checkOutButton;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static int sum=0;
    void order(String food,int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+ food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0){
            String currentText=ordersList.getText();
            ordersList.setText(currentText+food+" " + price +"yen (+tax)\n");
            sum+=price;
            total(sum);
            if(food.equals("Toast")){
                JOptionPane.showMessageDialog(null,
                        "Order for "+ food +" received.\nIf you order it with coffee,coffee will be half price!");
            }else if(food.equals("Coffee")){
                JOptionPane.showMessageDialog(null,
                        "Order for "+ food +" received.\nIf you order it with toast,coffee will be half price!");
            }else if(food.equals("Burger")){
                JOptionPane.showMessageDialog(null,
                        "Order for "+ food +" received.\nIf you order it with chicken,it will be 100 yen cheaper!");
            }else if(food.equals("Chicken")){
                JOptionPane.showMessageDialog(null,
                        "Order for "+ food +" received.\nIf you order it with burger,it will be 100 yen cheaper!");
            }else{
                JOptionPane.showMessageDialog(null,
                        "Order for "+ food +" received.");
            }
        }
    }
    void total(int sum){
        total.setText("Total     "+(int)(sum+sum*0.1)+" yen (including tax)");
    }
    public FoodOrderingMachine() {
        total.setText("Total     "+0+" yen (including tax)");
        sandButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sandwich",450);
            }
        });
        sandButton.setIcon(new ImageIcon(
                this.getClass().getResource("sand1.png")
        ));
        burgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Burger",550);
            }
        });
        burgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("burger1.png")
        ));
        toastButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Toast",350);
            }
        });
        toastButton.setIcon(new ImageIcon(
                this.getClass().getResource("toast1.png")
        ));
        chickenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chicken",400);
            }
        });
        chickenButton.setIcon(new ImageIcon(
                this.getClass().getResource("chicken1.png")
        ));
        hotdogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hotdog",350);
            }
        });
        hotdogButton.setIcon(new ImageIcon(
                this.getClass().getResource("hotdog1.png")
        ));
        coffeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coffee",300);
            }
        });
        coffeeButton.setIcon(new ImageIcon(
                this.getClass().getResource("coffee1.png")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0){
                    String menu=ordersList.getText();
                    if(menu.contains("Coffee") && menu.contains("Toast")) {
                        int confirmation1 = JOptionPane.showConfirmDialog(null,
                                "With the coffee & toast set discount,coffee will be half price!\nWould you like to discount?",
                                "Set Discount",
                                JOptionPane.YES_NO_OPTION
                        );
                        if (confirmation1 == 0) {
                            sum -= 150;
                        }
                    }
                    if (menu.contains("Burger") && menu.contains("Chicken")) {
                        int confirmation2 = JOptionPane.showConfirmDialog(null,
                                "You can save 100 yen with a set discount on burger & chicken!\nWould you like to discount?",
                                "Set Discount",
                                JOptionPane.YES_NO_OPTION
                        );
                        if (confirmation2 == 0) {
                            sum -= 100;
                        }
                    }
                    int confirmation3=JOptionPane.showConfirmDialog(null,
                            "Will you be eating here?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    if(confirmation3==0){
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is " + (int) (sum + sum * 0.1) + " yen(including tax).");
                    }else if(confirmation3==1){
                        JOptionPane.showMessageDialog(null,
                                "Thank you. It's take-out. The total price is " + (int) (sum + sum * 0.08) + " yen(including tax).");
                    }
                    ordersList.setText("");
                    total.setText("total   " + sum + "yen");
                    total(0);
                    sum = 0;
                }
            }
        });
    }
}
